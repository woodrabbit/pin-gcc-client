Name:           pin-gcc-client
Version:        0.4.1
Release:        12
Summary:        A Pin (Plug-IN framework) client is implemented based on GCC plugin and can execute the compiler optimization pass in GCC.
License:        GPLv3+ and GPLv3+ with exceptions and GPLv2+ with exceptions and LGPLv2+ and BSD
URL:            https://gitee.com/src-openeuler/pin-gcc-client
Source0:        %{name}-%{version}.tar.gz

BuildRequires:  gcc gcc-c++ gcc-plugin-devel cmake make pkgconfig grpc grpc-plugins grpc-devel protobuf-devel jsoncpp-devel
BuildRequires:  mlir mlir-static mlir-devel llvm-devel llvm-test
Requires:       gcc grpc protobuf

Patch1: 0001-Pin-gcc-client-BugFix-for-SwitchOp-change-it-to-term.patch
Patch2: 0002-Pin-gcc-client-Bugfix-for-GetFunctionById.patch
Patch3: 0003-Pin-gcc-client-Init-a-SimpleIPAPASS.patch
Patch4: 0004-Pin-gcc-client-Add-CGnodeOp.patch
Patch5: 0005-Pin-gcc-client-Add-support-for-decl-and-field-SetDec.patch
Patch6: 0006-Pin-gcc-client-Add-GetDeclType.patch
Patch7: 0007-Pin-gcc-client-Fix-VectorType.patch
Patch8: 0008-Pin-gcc-client-Fix-struct-self-contained-CallOp-Tree.patch
Patch9: 0009-Pin-gcc-client-Fix-TreeToValue-VAR_DECL-and-ARRAY_TY.patch
Patch10: 0010-PluginClient-Fix-the-bug-during-multi-process-compil.patch
Patch11: 0011-Pin-gcc-client-Adaptation-to-llvm15-mlir15-only-solv.patch
Patch12: 0012-Pin-gcc-client-Adaptation-to-gcc12-only-solves-the-b.patch

%description
A Pin (Plug-IN framework) client is implemented based on GCC plugin and can execute the compiler optimization pass in GCC.

%prep
%autosetup -p1

%build
mkdir -p _build
cd _build
%{cmake} .. -DCMAKE_INSTALL_PREFIX=%{_usr} -DCMAKE_INSTALL_LIBDIR=%{_libdir} -DCMAKE_SKIP_RPATH=ON -DMLIR_DIR=/usr/lib64/cmake/mlir -DLLVM_DIR=/usr/lib64/cmake/llvm
%make_build

%install
cd _build
%make_install

mkdir -p %{buildroot}/etc/ld.so.conf.d
echo "{_libdir}" > %{buildroot}/etc/ld.so.conf.d/%{name}-%{_arch}.conf

find %{buildroot} -type f -name "*.so" -exec strip "{}" ";"

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%license LICENSE
%attr(0755,root,root) %{_libdir}/libpin_gcc_client.so
%attr(0755,root,root) %{_libdir}/libMLIRClientAPI.so
%attr(0755,root,root) %{_libdir}/libMLIRClientAPI.so.15
%attr(0755,root,root) %{_libdir}/libMLIRPlugin.so
%attr(0755,root,root) %{_libdir}/libMLIRPlugin.so.15
%attr(0644,root,root) %{_bindir}/pin-gcc-client.json
%config(noreplace) /etc/ld.so.conf.d/%{name}-%{_arch}.conf

%changelog
* Tue Aug 22 2023 liyunfei <liyunfei33@huawei.com> - 0.4.1-12
- Type:FIX
- ID:NA
- SUG:NA
- DESC:FIX clang toolchain error

* Thu Aug 3 2023 dingguangya <dingguangya1@huawei.com> - 0.4.1-11
- Type:FIX
- ID:NA
- SUG:NA
- DESC:FIX STRIP problem

* Thu Aug 3 2023 dingguangya <dingguangya1@huawei.com> - 0.4.1-10
- Type:FIX
- ID:NA
- SUG:NA
- DESC:Adaptation to gcc12 only solves the build problem

* Thu Aug 3 2023 dingguangya <dingguangya1@huawei.com> - 0.4.1-9
- Type:FIX
- ID:NA
- SUG:NA
- DESC:Adaptation to llvm15/mlir15 only solves the build problem

* Tue May 09 2023 shenbowen <shenbowen@xfusion.com> - 0.4.1-8
- Type:SPEC
- ID:NA
- SUG:NA
- DESC:modify %patchxxx -p1 to %autosetup -p1

* Thu Apr 6 2023 dingguangya <dingguangya1@huawei.com> - 0.4.1-7
- Type:Update
- ID:NA
- SUG:NA
- DESC:Fix the bug during multi-process compilation

* Sat 25 2023 dingguangya <dingguangya1@huawei.com> - 0.4.1-6
- Type:Update
- ID:NA
- SUG:NA
- DESC:Fix TreeToValue-VAR_DECL and ARRAY_TYPE-getDomainIndex

* Fri Mar 17 2023 dingguangya <dingguangya1@huawei.com> - 0.4.1-5
- Type:Update
- ID:NA
- SUG:NA
- DESC:Fix VectorType CallOp TreeTOValue

* Tue Mar 7 2023 dingguangya <dingguangya1@huawei.com> - 0.4.1-4
- Type:Update
- ID:NA
- SUG:NA
- DESC:Sync patch from openEuler/pin-gcc-client

* Mon Mar 6 2023 wumingchuan <wumingchuan1992@foxmail.com> - 0.4.1-3
- Type:Update
- ID:NA
- SUG:NA
- DESC: Fix RPATH problem by ld.so.conf.d

* Fri Mar 3 2023 wumingchuan <wumingchuan1992@foxmail.com> - 0.4.1-2
- Type:Update
- ID:NA
- SUG:NA
- DESC: Fix STRIP & RPATH

* Sun Feb 26 2023 dingguangya <dingguangya1@huawei.com> - 0.4.1-1
- Type:Update
- ID:NA
- SUG:NA
- DESC:Update to v0.4.1

* Tue Dec 20 2022 zhaowenyu <804544223@qq.com> - 0.4.0-1
- Type:Update
- ID:NA
- SUG:NA
- DESC:Update to v0.4.0

* Thu Dec 08 2022 benniaobufeijiushiji <linda7@huawei.com> - 0.3.0-2
- Type:Sync
- ID:NA
- SUG:NA
- DESC:Sync patch from openEuler/pin-gcc-client

* Thu Dec 01 2022 wumingchuan <wumingchuan1992@foxmail.com> - 0.3.0-1
- Type:Update
- ID:NA
- SUG:NA
- DESC:Update to v0.3.0

* Thu Sep 29 2022 wumingchuan <wumingchuan1992@foxmail.com> - 0.2.0-1
- Type:Update
- ID:NA
- SUG:NA
- DESC:Update to v0.2.0

* Sat Aug 27 2022 liyancheng <412998149@qq.com> - 0.1.0-2
- Type:Bugfix
- ID:NA
- SUG:NA
- DESC:Static link plg_grpc_proto

* Fri Aug 26 2022 liyancheng <412998149@qq.com> - 0.1.0-1
- Type:Init
- ID:NA
- SUG:NA
- DESC:Init pin-gcc-client repository
